ActsAsTaggableOn extension for applying origin taggings to a model, specifically
extracted for tagging the origin of a submission (eg job application) object and
providing additional search and Ransack support.

This solves the outstanding Ransack Github issue [#135](https://github.com/activerecord-hackery/ransack/issues/135) (solution yet to be posted)

I had to extract this from a monolith piece of code so there are probably some
minor inconsistencies or potentially some minor missing parts but the overall
concept should still be assessable.

It was used in such a way to tag an incoming submission object with its origin.
The various scopes were provided for miscellaneous reporting or output.
The Ransack functionality is used something similar to:

```
  Submission.search("origins_api_eq" => true, "origins_form_eq" => true).result.to_sql

  =>

  SELECT `submissions`.*
  FROM `submissions`
  LEFT OUTER JOIN `taggings`
    ON `taggings`.`taggable_id` = `submissons`.`id`
    AND `taggings`.`context` = 'origins'
    AND `taggings`.`taggable_type` = 'Submission'
  LEFT OUTER JOIN `tags`
    ON `tags`.`id` = `taggings`.`tag_id`
  WHERE (
    ((`guests`.`id` = `guests`.`id` AND `guests`.`id` AND `taggings`.`tag_id` = -2)
    AND (`guests`.`id` = `guests`.`id` AND `guests`.`id` AND `taggings`.`tag_id` = -3))
  )
```

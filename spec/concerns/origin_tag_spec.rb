require 'spec_helper'

describe OriginTag do
  describe '.tags_from_source_name' do
    it 'should return the array of matching tags' do
      expect(described_class.tags_from_group_name('application')).to eq(['User', 'API'])
      expect(described_class.tags_from_group_name('imported file')).to eq(['File'])
      expect(described_class.tags_from_group_name('web form')).to eq(['Form'])
    end
  end

  describe '.source_name' do
    it 'should return the humanized name' do
      expect(described_class.source_name('User')).to eq('application')
      expect(described_class.source_name('API')).to eq('application')
      expect(described_class.source_name('File')).to eq('imported file')
      expect(described_class.source_name('Form')).to eq('web form')
    end
  end
end

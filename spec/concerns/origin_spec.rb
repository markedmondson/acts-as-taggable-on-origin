require 'acts_as_fu'
require 'spec_helper'
require 'acts-as-taggable-on'
require 'concerns/origin'

describe Origin do
  let(:submission) { Submission.new(name: 'Submission') }
  let(:user)       { User.new(name: 'User') }

  before { user.save }

  describe '.tag_origin' do
    it 'should add any tags by default' do
      submission.origin_list = 'API'
      submission.tag_origin('User')
      submission.save
      submission.reload
      expect(submission.origin_list).to eq(['API', 'User'])
      expect(Submission.tagged_with('User')).to include(submission)
    end

    context 'with an owner' do
      let(:owner)  { User.new(name: 'Owner') }

      before { owner.save }

      it 'should update an origin tag without a tagger' do
        submission.tag_origin('API')
        submission.save!
        submission.reload
        submission.tag_origin('API', owner)
        submission.save!
        submission.reload
        expect(submission.owner_tags_on(owner, :origins).first.name).to eq('API')
        expect(Submission.tagged_with('API')).to include(submission)
      end

      it 'should update an existing tag' do
        submission.tag_origin('API', user)
        submission.save!
        submission.reload
        submission.tag_origin('API', owner)
        submission.save!
        submission.reload
        expect(submission.owner_tags_on(user, :origins)).to eq([])
        expect(submission.owner_tags_on(owner, :origins).first.name).to eq('API')
      end

      it 'should apply any tags and owner' do
        submission.tag_origin('API', user)
        submission.save!
        submission.reload
        expect(submission.owner_tags_on(user, :origins).first.name).to eq('API')
        expect(Submission.tagged_with('API')).to include(submission)
      end
    end
  end

  describe '.excluding_submitted_origin' do
    let(:api_submission)  { Submission.new(name: 'API', origin_list: 'API') }
    let(:form_submission) { Submission.new(name: 'Form', origin_list: 'Form') }
    let(:none_submission) { Submission.new(name: 'None') }

    before do
      none_submission.origin_list = []
      api_submission.save
      form_submission.save
      none_submission.save
    end

    it 'should exclude the form origin' do
      expect(Submission.excluding_submitted_origin).not_to include form_submission
    end

    it 'should include the untagged submission' do
      expect(Submission.excluding_submitted_origin).to include none_submission
    end

    it 'should include the api submission' do
      expect(Submission.excluding_submitted_origin).to include api_submission
    end
  end

  describe '.without_origin' do
    let(:file_submission) { Submission.new(name: 'File', origin_list: 'File') }
    let(:none_submission) { Submission.new(name: 'None') }

    before do
      none_submission.origin_list = []
      file_submission.save
      none_submission.save
    end

    it 'should exclude the file origin' do
      expect(Submission.excluding_submitted_origin).not_to include file_submission
    end

    it 'should include the untagged submission' do
      expect(Submission.excluding_submitted_origin).to include none_submission
    end
  end

  context 'tags' do
    before do
      submission.origin_list = 'API'
      submission.save!
      submission.reload
    end

    describe '#from_origin?' do
      it 'should return true if it includes the tag' do
        expect(submission.from_origin?('API')).to eq(true)
        expect(submission.from_origin?('Form')).to eq(false)
      end
    end

    describe '#last_origin' do
      it 'should return the last tag' do
        expect(submission.last_origin).to eq(ActsAsTaggableOn::OriginTag.fixed_tags[:api])
      end
    end

    describe '#last_origin_source_name' do
      it 'should return the source name of the last tag' do
        expect(submission.last_origin_group_name).to eq('application')
      end
    end
  end

end

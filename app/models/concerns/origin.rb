require 'acts-as-taggable-on'

##
# Extension module to add origin tagging support to any multiple models
module Origin
  extend ActiveSupport::Concern

  included do
    acts_as_taggable_on :origins

    begin
      with_options through: :origin_taggings, source: :tag, class_name: 'ActsAsTaggableOn::OriginTag' do |o|
        o.has_many :origin_api,   conditions: { taggings: { tag_id: [OriginTag.user.id, OriginTag.api.id] }}
        o.has_many :origin_form,  conditions: { taggings: { tag_id: OriginTag.form.id, tagger_id: nil } }
        o.has_many :origin_file,  conditions: { taggings: { tag_id: OriginTag.file.id } }
      end
    rescue ActiveRecord::StatementInvalid
      logger.warn 'Skipping origin associations on #{self} (tables not yet created?)'
    end

    # Search for items tagged with this origin tag
    # @param [Array,String] origin
    #
    scope :from_origin, ->(origin) {
      tagged_with(origin, on: :origins, any: 'distinct')
    }

    # Exclude any origins that originated from a user submission (form or file)
    #
    scope :excluding_submitted_origin, -> {
      tt = ActsAsTaggableOn::Tagging.arel_table
      form_tag_id = OriginTag.form.id
      file_tag_id = OriginTag.file.id

      pred_2 = tt[:tag_id].not_eq(form_tag_id)
      pred_3 = tt[:tag_id].not_eq(file_tag_id)

      without_origin.or(
        tt.grouping(pred_2.and(pred_3))
      )
    }

    # Search for orphaned items without origin
    #
    scope :without_origin, -> {
      tt = ActsAsTaggableOn::Tagging.arel_table
      eq_model_id   = tt[:taggable_id].eq(arel_table[:id])
      eq_model_type = tt[:taggable_type].eq(self.name)
      eq_context    = tt[:context].eq('origins')
      on_model      = Arel::Nodes::On.new(eq_model_id.and(eq_model_type).and(eq_context))

      joins(Arel::Nodes::OuterJoin.new(tt, on_model)).where(
        tt[:taggable_id].eq(nil)
      )
    }
  end

  # Tag with the origin list, attached to the owner if present
  # Autosave is not enabled for acts_as_taggable_on so we have to save
  # updated taggings manually
  # @param [String] origin The origin string, defaults to 'User'
  # @param [User] owner origin tag owner
  # @return [Array]
  #
  def tag_origin(origin='User', owner=nil)
    existing_origin = origin_taggings.detect { |tagging|
      OriginTag.fixed_tags.values.detect { |tag| tagging.tag_id == tag.id }
    }
    if existing_origin && owner
      # This may be an orphan origin tag (such as migrated records), update with new owner
      existing_origin.assign_attributes(tagger_id: owner.id, tagger_type: owner.class.name)
      existing_origin.save(validate: false)
    elsif existing_origin.nil?
      association(:origin_taggings).reset
      if owner
        owner.tag(self, with: origin, on: :origins, skip_save: true)
      else
        origin_list.add(origin)
      end
    end
  end

  # Whether this contains the origin tag (doesn't care whether the origin has a tagger or not)
  # @param [String] origin
  # @return [Boolean]
  #
  def from_origin?(origin)
    all_origins_list.include?(origin)
  end

  # Should return the subscriber group: PMS/API => reservation, Form => list signup
  # @return [String]
  #
  def origin_name
    origins.map(&:source_name).uniq.join(', ')
  end

  # Most recent origin
  # @return [Origin]
  #
  def last_origin
    origins.order(::ActsAsTaggableOn::Tagging.arel_table[:created_at].asc).first
  end

  # Get the a name of the most recent origin
  # @return [String]
  #
  def last_origin_source_name
    if last_origin
      name = last_origin.name.downcase
      ActsAsTaggableOn::OriginTag.send(name).source_name if ActsAsTaggableOn::OriginTag.respond_to?(name)
    end
  end

  # Override acts_as_taggable_on because the original method redefines all associations
  # when called, which overwrites the changes made through Model -> ActsAsTaggableOn::Origin -> has_many :origins
  module ClassMethods
    # Override to reset our preferred class
    def acts_as_taggable_on(*tag_types)
      super
      reflections[:origins].options[:class_name] = 'ActsAsTaggableOn::OriginTag'
    end
  end
end

require 'acts-as-taggable-on'
require 'ransack'

module ActsAsTaggableOn
  class OriginTag < ::ActsAsTaggableOn::Tag

    FIXED_TAG_ATTRS = {
      user: { id: -1, name: 'User' },
      api:  { id: -2, name: 'API' },
      form: { id: -3, name: 'Form' },
      file: { id: -4, name: 'File' },
    }.freeze

    # Call and save the seed origin tags
    #
    def self.fixed_tags
      @fixed_tags ||= Hash[
        FIXED_TAG_ATTRS.map { |key, attrs|
          tag = where(id: attrs[:id], name: attrs[:name]).first_or_create
          tag.readonly!
          [key, tag]
        }
      ].freeze
    end

    FIXED_TAG_ATTRS.each do |key, tag_attrs|
      define_singleton_method key do
        fixed_tags[key]
      end
    end

    # Define ransackers
    taggable_identity = ->(parent) { Submission.arel_table[:id] }

    ransacker :user, type: :boolean_value,
              formatter: ->(v) { scope_from_tag([user, api], v) },
              callable: taggable_identity

    ransacker :file, type: :boolean_value,
              formatter: ->(v) { scope_from_tag(file, v) },
              callable: taggable_identity

    ransacker :form, type: :boolean_value,
              formatter: ->(v) { scope_from_tag(form, v, with_tagger: true) },
              callable: taggable_identity

    # Convert the search value to an arel node as "(no-op) AND (tagged)"
    # For patching into the right hand value of an id equality: we stuff the
    # original condition with "id = id" to ignore it and chain ours after it.
    # @param [String,Array]
    # @param [Boolean]
    #
    def self.scope_from_tag(tags, value, *args)
      options = args.extract_options!
      tag_ids = Array(tags).map(&:id)
      tag_ids = tag_ids.first if tag_ids.size == 1

      tt = ::ActsAsTaggableOn::Tagging.arel_table
      scope = Submission.joins(:origins).where(taggings: { tag_id: tag_ids })
      scope = scope.where(taggings: { tagger_id: nil }) if options[:with_tagger]
      scope = scope.where_values.first
      scope = scope.not.or(tt[:tag_id].eq(nil)) if Ransack::Constants::FALSE_VALUES.include?(value)
      arel_table.create_and([Submission.arel_table[:id], scope])
    end

    # Find origin tags for a humanized source name
    # Given that an API submission gives us "application", then we want
    # everything from "User" and "API" so we can search for both
    # @param [String] readable source name
    # @return [Array<String>]
    #
    def self.tags_from_source_name(tag_source)
      fixed_tags.values.each_with_object([]) do |t, a|
        a << t.name if t.source_name == tag_source
      end
    end

    # Humanize the tag group name
    # TODO: Better in a decorator?
    #
    def source_name
      case name
      when 'User', 'API'
        'application'
      when 'File'
        'imported file'
      when 'Form'
        'web form'
      else
        name
      end
    end
  end
end

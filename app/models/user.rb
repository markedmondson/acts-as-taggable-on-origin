require 'acts-as-taggable-on'

class User < ActiveRecord::Base
  acts_as_tagger
end

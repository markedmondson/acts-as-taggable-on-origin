require 'ransack'

class Submission < ActiveRecord::Base
  include Origin

  has_ransackable_associations %w(origins)
end
